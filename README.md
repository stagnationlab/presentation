# Presentation
**Stagnation Lab themed presentation base using [remark](https://github.com/gnab/remark).**

## How to use
- See `presentation.md` as reference presentation.
- Formatted using [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) syntax with some [sugar on top](https://github.com/gnab/remark/wiki/Markdown).
- To create a new presentation, create a new branch in the repository. This way You can always update the base styles if they change.
- Read [the manual](https://github.com/gnab/remark/wiki) to learn the syntax or just follow the example.

## How to run
- needs [nodejs](https://nodejs.org/en/)
- `git clone git@bitbucket.org:stagnationlab/presentation.git` to clone the repo (or use GitKraken etc)
- `yarn install` to install dependencies (or `npm install` if not using yarn yet)
- `yarn start` to start local server and open the presentation in the browser (or just open `present.cmd`)

## Available theme classes
- `center` to center text horizontally
- `middle` to center text vertically
- `start` for the first slide
- `end` for the last slide
- `colored brand` to use main brand colored background
- `colored yellow` to use yellow colored background
- `colored green` to use green colored background
- `colored blue` to use blue colored background
- `colored magenta` to use magenta colored background
- `colored red` to use red colored background

## Technical stuff
- the custom styling is defined in `index.html`