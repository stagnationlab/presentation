class: start

# Stagnation Lab
Priit Kallas<br>
priit@stagnationlab.com

---
class: colored brand

## Brand

1. This
2. Is
3. Ordered
4. List

---
class: colored yellow

## Yellow

- This
- Is
- Unordered
- List

---
class: colored green

## Green

### Sub heading
- This text **is bold**
- This text *is cursive*
- This text is ~~striked through~~

---
class: colored blue

## Blue

Image example
![alt text](http://placekitten.com.s3.amazonaws.com/homepage-samples/408/287.jpg "Logo Title Text 1")

---
class: colored magenta

## Magenta
Code example:
```javascript
var slideshow = remark.create({
  sourceUrl: 'presentation.md',
  ratio: '16:9'
});
```

---
class: colored red

## Red
Table example:

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

---
class: end

## The end
priit@stagnationlab.com
